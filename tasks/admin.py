from django.contrib import admin
from .models import Task

# Register your models here.


@admin.register(Task)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "project",
        "is_completed",
        "start_date",
        "due_date",
        "assignee",
    )
